
// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');
const persistenceAdapter = require('ask-sdk-s3-persistence-adapter')

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    async handle(handlerInput) {
        
        const attributesManager = handlerInput.attributesManager;
        const sessionAttributes = attributesManager.getSessionAttributes() || {};

        //const year = sessionAttributes.hasOwnProperty('year') ? sessionAttributes.year : 0;
        //const month = sessionAttributes.hasOwnProperty('month') ? sessionAttributes.month : 0;
        //const day = sessionAttributes.hasOwnProperty('day') ? sessionAttributes.day : 0;
        
        //const year = d.getFullYear();
        const month = 12;
        const day = 25;
        
        const serviceClientFactory = handlerInput.serviceClientFactory;
        const deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        
        const upsServiceClient = serviceClientFactory.getUpsServiceClient();
        const userTimeZone = await upsServiceClient.getSystemTimeZone(deviceId);
        
        console.log("timezone", userTimeZone);
            
        const oneDay = 24*60*60*1000;
        
        // getting the current date with the time
        const currentDateTime = new Date(new Date().toLocaleString("en-US", {timeZone: userTimeZone}));
        // removing the time from the date because it affects our difference calculation
        const currentDate = new Date(currentDateTime.getFullYear(), currentDateTime.getMonth(), currentDateTime.getDate());
        let currentYear = currentDate.getFullYear();
        
        console.log('currentDateTime:', currentDateTime);
        console.log('currentDate:', currentDate);
        
        // getting the next birthday
        let nextBirthday = Date.parse(`${month} ${day}, ${currentYear}`);
        
        // adjust the nextBirthday by one year if the current date is after their birthday
        if (currentDate.getTime() > nextBirthday) {
            nextBirthday = Date.parse(`${month} ${day}, ${currentYear + 1}`);
            currentYear++;
        }
        
        // setting the default speakOutput to Happy xth Birthday!! 
        // Alexa will automatically correct the ordinal for you.
        // no need to worry about when to use st, th, rd
        //let speakOutput = currentYear - year}th birthday!`;
        let speakOutputLast = ' This is Christmas calendar app. Would you like to start your app?';
        let speakOutput = `Hello, This is a Christmas day!` + speakOutputLast;
        if (currentDate.getTime() !== nextBirthday) {
            const diffDays = Math.round(Math.abs((currentDate.getTime() - nextBirthday)/oneDay));
            //speakOutput = `Welcome back. It looks like there are ${diffDays} days until your ${currentYear - year}th birthday.`
            speakOutput = `Hello, It looks like there are ${diffDays} days until Christmas.` + speakOutputLast
        }
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};


// Calendar intent allowing users to save the schedule.
const InProgressSaveCalendarIntentHandler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        console.log("Hello, this is the test log output.")
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SaveCalendarIntent'
            && Alexa.getDialogState(handlerInput.requestEnvelope) !== 'COMPLETED';
    },
    
     handle(handlerInput) {
         
        const currentIntent = handlerInput.requestEnvelope.request.intent
        console.log("currentIntent", currentIntent);
        console.log("handlerInput", handlerInput);
        let prompt = '';
        let prompt_final_speech = '';
        let reprompt_speech = 'Sorry, I could not hear you. Could you answer my question?';
        //for (const slotName of Object.keys(handlerInput.requestEnvelope.request.intent.slots)) 
        for (var i = 0; i < requiredSlots.length; i++)
        {
          var slotName = requiredSlots[i];  
          console.log("slotName", slotName);
          const currentSlot = currentIntent.slots[slotName];
          console.log(currentSlot, "currentSlot");
          
          // If a user is not provide the data, then it will go to the initial question.
          if ((currentSlot.value === "?" || currentSlot.value === undefined) && (currentSlot.source !== undefined || currentSlot.confirmationStatus !== 'NONE')
                    ) {
                let prompt = ''
                switch (currentSlot.name){
                    case "streetName":
                        prompt = `What street will you go to attend event?`;
                        break;
                    case "state":
                        prompt = `What suburb will you go to for the event?`;
                        break;
                    case "event":
                        prompt = `What kind of ${currentSlot.name } are you going to?`;
                        break
                    default:
                        prompt = `What ${currentSlot.name} will you attend event?`;
                    
                }
                return handlerInput.responseBuilder
                      .speak(prompt)
                      .reprompt(prompt)
                      .addElicitSlotDirective(currentSlot.name)
                      .getResponse();  
                }
            
            // If not and the user has already provided the information. 
        else if (currentSlot.source !== undefined){
                if (requiredSlots.indexOf(currentSlot.name) > -1) {
                    switch (currentSlot.name) {
                        case "year":
                        
                        // Check if year is correct
                            if (parseInt(currentSlot.value) > 2100) {
                                return handlerInput.responseBuilder
                                  .speak("Your year is too large. Please tell me your year again.")
                                  .addElicitSlotDirective(currentSlot.name)
                                  .getResponse(); 
                            }
                            break;
                            
                        // Check if month is correct
                        case "month":
                            if(Object.keys(monthToValue).indexOf(currentSlot.value) < 0) {
                                return handlerInput.responseBuilder
                                  .speak("Your month is incorrect. Please tell me your month again.")
                                  .addElicitSlotDirective(currentSlot.name)
                                  .getResponse(); 
                            }
                            break;
                            
                        // Check if date is correct.
                        case "date":
                            var currentYear = parseInt(currentIntent.slots["year"].value);
                            var currentMonth = parseInt(monthToValue[currentIntent.slots["month"].value]);
                            var d = new Date(currentYear, currentMonth + 1, 0);
                            /*
                            console.log("currentMonth", currentMonth);
                            console.log("currentYear", currentYear);
                            console.log("Date", d.getDate());
                            */
                            if (parseInt(currentSlot.value) > d.getDate()){
                                return handlerInput.responseBuilder
                                  .speak("Your date is too large. Please tell me your month again.")
                                  .addElicitSlotDirective(currentSlot.name)
                                  .getResponse(); 
                            }
                            break;
                            
                        default:
                            console.log("Time");
                    }
                }                
            }
        }
    console.log("handlerInput", handlerInput);
    console.log("sessionAttributes", handlerInput.attributesManager.getSessionAttributes());
    console.log("currentIntent", currentIntent);
    return handlerInput.responseBuilder
        .speak(prompt_final_speech)
        .reprompt(reprompt_speech)
        .addDelegateDirective(currentIntent)
        .getResponse();
   
    }   
};


const CompleteProgressSaveCalendarIntentHandler =  {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        console.log("Hello, this is the test log output2222");
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
        && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SaveCalendarIntent'
        &&    
        Alexa.getDialogState(handlerInput.requestEnvelope) === 'COMPLETED'
    },
    
    async handle(handlerInput) {
        
        const attributesManager = handlerInput.attributesManager; 
        
        const year = handlerInput.requestEnvelope.request.intent.slots.year.value;
        const month = handlerInput.requestEnvelope.request.intent.slots.month.value;
        const day = handlerInput.requestEnvelope.request.intent.slots.date.value; 
        const time = handlerInput.requestEnvelope.request.intent.slots.time.value;
        const streetName = handlerInput.requestEnvelope.request.intent.slots.streetName.value;
        const region = handlerInput.requestEnvelope.request.intent.slots.region.value;
        const event = handlerInput.requestEnvelope.request.intent.slots.event.value;
        
        const scheduleAttributes = {
                                    "year" : year,
                                    "month" : month,
                                    "day" : day,
                                    "time": time,
                                    "street": streetName,
                                    "region": region,
                                    "event": event
                                    };
        
        attributesManager.setPersistentAttributes(scheduleAttributes);
        await attributesManager.savePersistentAttributes();
        
        console.log("slots_completion",handlerInput.requestEnvelope.request.intent.slots);
        let speakOutput = 'Your schedule has been saved. Do you have anything you would like to do?';
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};


const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'You can say hello to me! How can I help?';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Goodbye!';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};


const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.stack}`);
        const speakOutput = `Sorry, I had trouble doing what you asked. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

/* HELPER FUNCTIONS */

/* Get Slot Functions */
function getSlotValues(filledSlots) {
  const slotValues = {};

  console.log(`The filled slots: ${JSON.stringify(filledSlots)}`);
  Object.keys(filledSlots).forEach((item) => {
    const name = filledSlots[item].name;

    if (filledSlots[item] &&
      filledSlots[item].resolutions &&
      filledSlots[item].resolutions.resolutionsPerAuthority[0] &&
      filledSlots[item].resolutions.resolutionsPerAuthority[0].status &&
      filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {
      switch (filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {
        case 'ER_SUCCESS_MATCH':
          slotValues[name] = {
            synonym: filledSlots[item].value,
            resolved: filledSlots[item].resolutions.resolutionsPerAuthority[0].values[0].value.name,
            isValidated: true,
          };
          break;
        case 'ER_SUCCESS_NO_MATCH':
          slotValues[name] = {
            synonym: filledSlots[item].value,
            resolved: filledSlots[item].value,
            isValidated: false,
          };
          break;
        default:
          break;
      }
    } else {
      slotValues[name] = {
        synonym: filledSlots[item].value,
        resolved: filledSlots[item].value,
        isValidated: false,
      };
    }
  }, this);

  return slotValues;
}

const requiredSlots = [
  'year',
  'month',
  'date',
  'time',
  "streetName",
  'region',
  'event'
];


const monthToValue = {"January": 0,
    "February": 1,
    "March": 2,
    "April": 3,
    "May": 4,
    "June": 5,
    "July": 6,
    "August": 7,
    "September": 8,
    "October": 9,
    "November": 10,
    "December": 11}

exports.handler = Alexa.SkillBuilders.custom()
    .withPersistenceAdapter(
    new persistenceAdapter.S3PersistenceAdapter({bucketName:process.env.S3_PERSISTENCE_BUCKET})
    )
    .addRequestHandlers(
        LaunchRequestHandler,
        //CalendarStartIntentHandler,
        InProgressSaveCalendarIntentHandler,
        CompleteProgressSaveCalendarIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler, // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    )
    .addErrorHandlers(
        ErrorHandler,
    )
    .withApiClient(new Alexa.DefaultApiClient())
    .lambda();
